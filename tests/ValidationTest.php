<?php

use Tingg\Checkout\Mock;
use Tingg\Checkout\Validation;
use Tingg\Checkout\Constants;
use PHPUnit\Framework\TestCase;

final class ValidationTest extends TestCase
{
    public function testIsValidUrl()
    {
        // Negative tests
        $this->assertFalse(Validation::is_valid_url("/callback"));
        $this->assertFalse(Validation::is_valid_url("example.com"));
        $this->assertFalse(Validation::is_valid_url("/callback?action=acknowldege"));

        // Positive tests
        $this->assertTrue(Validation::is_valid_url("http://example.com"));
        $this->assertTrue(Validation::is_valid_url("https://example.com"));
        $this->assertTrue(Validation::is_valid_url("https://example.com/callback"));
        $this->assertTrue(Validation::is_valid_url("https://example.com?action=reject"));
        $this->assertTrue(Validation::is_valid_url("https://example.com/?action=accept"));
    }

    public function testIsValidEmail()
    {
        // Negative tests
        $this->assertFalse(Validation::is_valid_email('john.doe'));
        $this->assertFalse(Validation::is_valid_email('@john.doe'));
        $this->assertFalse(Validation::is_valid_email('john.doe@example'));

        // Positive test
        $this->assertTrue(Validation::is_valid_email('john.doe@example.com'));
    }

    public function testIsValidAmount()
    {
        // Negative tests
        $this->assertFalse(Validation::is_valid_amount(NAN));
        $this->assertFalse(Validation::is_valid_amount(NULL));
        $this->assertFalse(Validation::is_valid_amount(-100));
        $this->assertFalse(Validation::is_valid_amount("1,000"));
        $this->assertFalse(Validation::is_valid_amount(INF));
        $this->assertFalse(Validation::is_valid_amount(""));
        $this->assertFalse(Validation::is_valid_amount(['amount' => 100]));
        // $this->assertFalse(Validation::is_valid_amount(PHP_FLOAT_MAX));

        // Positive tests
        $this->assertTrue(Validation::is_valid_amount(100));
        $this->assertTrue(Validation::is_valid_amount(1e12));
        $this->assertTrue(Validation::is_valid_amount("1e12"));
        $this->assertTrue(Validation::is_valid_amount(10e-1));
        $this->assertTrue(Validation::is_valid_amount(100.55));
    }

    public function testIsValidDueDate()
    {
        // Negative tests
        $this->assertFalse(Validation::is_valid_due_date("2029-12-31T23:59:59"));
        $this->assertFalse(Validation::is_valid_due_date("2029-12-31T23:59:59.000"));
        $this->assertFalse(Validation::is_valid_due_date("Mon Dec 31 2029 23:59:59 GMT+0000 (Coordinated Universal Time)"));

        // Positive tests
        // C language & PHP
        $this->assertTrue(Validation::is_valid_due_date("2029-12-31 23:59:59"));
    }

    public function testIsValidMsisdn()
    {
        // Negative tests
        $this->assertFalse(Validation::is_valid_msisdn("+254700000000", "KE"));
        $this->assertFalse(Validation::is_valid_msisdn("+254700000000", ""));
        $this->assertFalse(Validation::is_valid_msisdn("+254700000000", NULL));
        $this->assertFalse(Validation::is_valid_msisdn("+255700000000", "KEN"));

        // Positive tests
        $this->assertTrue(Validation::is_valid_msisdn("+254700000000", "KEN"));
        $this->assertTrue(Validation::is_valid_msisdn("+254 700000000", "KEN"));
        $this->assertTrue(Validation::is_valid_msisdn("(+254)700000000", "KEN"));
        $this->assertTrue(Validation::is_valid_msisdn("+254-700-000-000", "KEN"));
        $this->assertTrue(Validation::is_valid_msisdn("+254 700 000 000", "KEN"));
        $this->assertTrue(Validation::is_valid_msisdn("+255700000000", "TZA"));
    }

    public function testIsValidString()
    {
        // Negative tests
        $this->assertFalse(Validation::is_valid_string(""));
        $this->assertFalse(Validation::is_valid_string([]));
        $this->assertFalse(Validation::is_valid_string((object)[]));
        $this->assertFalse(Validation::is_valid_string(new DateTime()));

        // Positive tests
        $this->assertTrue(Validation::is_valid_string(10));
        $this->assertTrue(Validation::is_valid_string("10"));
        $this->assertTrue(Validation::is_valid_string("{}"));
        $this->assertTrue(Validation::is_valid_string("[]"));
        $this->assertTrue(Validation::is_valid_string("abc"));
    }

    public function testIsValidCountryCode()
    {
        // Negative tests
        $this->assertFalse(Validation::is_valid_country_code("KE"));
        $this->assertFalse(Validation::is_valid_country_code("404"));
        $this->assertFalse(Validation::is_valid_country_code("Kenya"));
        $this->assertFalse(Validation::is_valid_country_code("USA"));

        // Positive tests
        $this->assertTrue(Validation::is_valid_country_code("MWI"));
        $this->assertTrue(Validation::is_valid_country_code("KEN"));
        $this->assertTrue(Validation::is_valid_country_code("GHA"));
        $this->assertTrue(Validation::is_valid_country_code("ZMB"));
        $this->assertTrue(Validation::is_valid_country_code("UGA"));
        $this->assertTrue(Validation::is_valid_country_code("BWA"));
        $this->assertTrue(Validation::is_valid_country_code("AGO"));
        $this->assertTrue(Validation::is_valid_country_code("TZA"));
        $this->assertTrue(Validation::is_valid_country_code("NGA"));
        $this->assertTrue(Validation::is_valid_country_code("ZAF"));
        $this->assertTrue(Validation::is_valid_country_code("CIV"));
    }

    public function testIsValidCurrencyCode()
    {
        // Negative tests
        $this->assertFalse(Validation::is_valid_currency_code("KE"));
        $this->assertFalse(Validation::is_valid_currency_code("Kenya"));
        $this->assertFalse(Validation::is_valid_currency_code("USD"));

        // Positive tests
        $this->assertTrue(Validation::is_valid_currency_code("MWK"));
        $this->assertTrue(Validation::is_valid_currency_code("KES"));
        $this->assertTrue(Validation::is_valid_currency_code("GHS"));
        $this->assertTrue(Validation::is_valid_currency_code("ZMW"));
        $this->assertTrue(Validation::is_valid_currency_code("UGX"));
        $this->assertTrue(Validation::is_valid_currency_code("BWP"));
        $this->assertTrue(Validation::is_valid_currency_code("AOA"));
        $this->assertTrue(Validation::is_valid_currency_code("TZS"));
        $this->assertTrue(Validation::is_valid_currency_code("NGN"));
        $this->assertTrue(Validation::is_valid_currency_code("ZAR"));
        $this->assertTrue(Validation::is_valid_currency_code("XOF"));
    }

    public function testIsValidJson()
    {
        // Negative tests
        $this->assertFalse(Validation::is_valid_json(""));
        $this->assertFalse(Validation::is_valid_json("1"));
        $this->assertFalse(Validation::is_valid_json("true"));
        $this->assertFalse(Validation::is_valid_json("null"));
        $this->assertFalse(Validation::is_valid_json("hello"));
        $this->assertFalse(Validation::is_valid_json("{tingg}"));
        $this->assertFalse(Validation::is_valid_json("undefined"));

        $this->assertFalse(Validation::is_valid_json("['tingg']"));
        $this->assertFalse(Validation::is_valid_json('{name: "tingg"}'));

        // Positive tests
        $this->assertTrue(Validation::is_valid_json("[]"));
        $this->assertTrue(Validation::is_valid_json("{}"));
        $this->assertTrue(Validation::is_valid_json('["tingg"]'));
        $this->assertTrue(Validation::is_valid_json('{"name": "tingg"}'));
    }

    public function testIsValidBeneficiary()
    {
        // Negative tests
        $this->assertFalse(Validation::is_valid_beneficiary(null));
        $this->assertFalse(Validation::is_valid_beneficiary("TAX"));
        $this->assertFalse(Validation::is_valid_beneficiary([['amount' => 1, 'beneficiary_code' => 'TAX']]));
        $this->assertFalse(Validation::is_valid_beneficiary(['amt' => 1, 'charge_beneficiary_code' => 'TAX']));
        $this->assertFalse(Validation::is_valid_beneficiary(['amount' => 1, 'charge_beneficiary_code' => 'TAX']));

        // Positive tests
        $this->assertTrue(Validation::is_valid_beneficiary([['amount' => 1, 'charge_beneficiary_code' => 'TAX'], ['amount' => 2, 'charge_beneficiary_code' => 'COMMISSION']]));
    }

    /**
     * @dataProvider invalidClassConstructorTestData
     */
    public function testInvalidClassConstructor($payload)
    {
        $this->expectException(InvalidArgumentException::class);
        new Validation($payload);
    }

    public static function invalidClassConstructorTestData()
    {
        return [
            [null],
            [1234567890],
            ["qwertyuiop"]
        ];
    }

    public function testValidClassConstructor()
    {
        $this->expectNotToPerformAssertions();
        $instanceFromArr = new Validation(Mock::PAYLOAD_ARR());
        $instanceFromJson = new Validation(json_encode(Mock::PAYLOAD_ARR(), true));
    }

    public function testValidPayload()
    {
        $validation = (new Validation(Mock::PAYLOAD_ARR()))->validate();
        $this->assertTrue(isset($validation["data"]));
        $this->assertFalse(isset($validation["error"]));
    }
}
