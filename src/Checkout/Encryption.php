<?php

namespace Tingg\Checkout;

class Encryption
{
    private $iv;
    private $secret;

    const OPTIONS = 0;
    const CIPHER_ALGO = 'AES-256-CBC';
    public function __construct($iv_key, $secret_key)
    {
        if (is_string($iv_key) && strlen($iv_key) !== 16) throw new \InvalidArgumentException("IV key should be a 16 character string");

        if (is_string($secret_key) && strlen($secret_key) !== 16) throw new \InvalidArgumentException("Secret key should be a 16 character string");

        $this->secret = hash('sha256', $secret_key);
        $this->iv = substr(hash('sha256', $iv_key), 0, 16);
    }

    public function encrypt($payload): string
    {
        $data = json_encode($payload, true);

        $encrypted = openssl_encrypt($data, self::CIPHER_ALGO, $this->secret, self::OPTIONS, $this->iv);

        return base64_encode($encrypted);
    }
}