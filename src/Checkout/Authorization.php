<?php

namespace Tingg\Checkout;

class Authorization
{
    private $client_id;
    private $client_secret;
    private $environment;

    public function __construct($client_id, $client_secret, $environment)
    {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->environment = $environment;
    }

    public function accessToken()
    {
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => Constants::CAS_URL[$this->environment],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('grant_type' => Constants::CAS_GRANT_TYPE, 'client_id' => $this->client_id, 'scope' => '*', 'client_secret' => $this->client_secret)
            ));

            $response = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            if($httpCode != 200) return ["error" => ["message" => "Authentication failed. Check your credentials, and try again."]];

            $accessTokenResponse = json_decode($response,true);

            if(!isset($accessTokenResponse['access_token']))   return ["error" => ["message" => $accessTokenResponse['error']]];

            return $accessTokenResponse['access_token'];
        } catch (\Exception $ex) {
            var_dump($ex->getTraceAsString());
            return [
                "error" => ["message" => "Sorry, unable to get a access token " . $ex->getMessage()]
            ];
        }
    }
}

