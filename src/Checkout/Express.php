<?php

namespace Tingg\Checkout;
use Tingg\Checkout\Constants as Constants;
use Tingg\Checkout\Validation as Validation;
use Tingg\Checkout\Authorization as Authorization;
class Express
{
    public function create($payload,$environment,$client_id,$client_secret)
    {
        try {
//validate payload
            if (empty($client_id))
                return ["error" => ["message" => "ClientId should not be empty."]];

            if (empty($client_secret))
                return ["error" => ["message" => "Client Secret should not be empty."]];


            if (empty($payload))
                return ["error" => ["message" => "Payload should not be empty."]];

            if (!is_array($payload) && json_decode($payload,true) == null)
                return ["error" => ["message" => "Invalid payload. JSON string or associative array expected"]];

            if (!in_array($environment, Constants::SUPPORTED_ENVIRONMENTS, true))
                return ["error" => ["message" => "The environment should be one of " . implode(", ", Constants::SUPPORTED_ENVIRONMENTS)]];

            //fetch access token from cas
            $token = (new Authorization($client_id, $client_secret, $environment))->accessToken();

            if (isset($token['error']))  return ["error" => ["message" => $token["error"]["message"]]];

           //invoke checkout
            return $this->curlPostToURL(Constants::EXPRESS_URL[$environment], $payload, $token);

        } catch (\Exception $ex) {
            var_dump($ex->getTraceAsString());
            return [
                "message" => ["message" => "Sorry, unable to create a checkout request"]
            ];
        }

    }
    public function curlPostToURL($url,$payload,$token) {
        try{
        $authorization = "Authorization: Bearer ".$token;
        $payload = json_encode($payload);
        $header = array(
            "Content-type: application/json;charset=\"utf-8\"",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            $authorization
        );
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_URL, $url);
        curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($channel, CURLOPT_TIMEOUT, 60);
        curl_setopt($channel, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($channel, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($channel, CURLOPT_HTTPHEADER, $header);
        curl_setopt($channel, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($channel, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($channel, CURLOPT_VERBOSE, true);

        $response = curl_exec($channel);
        $httpCode = curl_getinfo($channel, CURLINFO_HTTP_CODE);
        curl_close($channel);
        if( $response == null)return [ "error" => ["message" => "No Response from express checkout."]];
        $response =  json_decode($response,true);
         return $this->handleResponse($httpCode,$response);
        } catch (\Exception $ex) {
            var_dump($ex->getTraceAsString());
            return [
                "error" => ["message" => "Sorry, unable to create a checkout request ".$ex->getMessage()]
            ];
        }
    }

    private function handleResponse($httpCode,$response): array
    {
        // Handle auth errors
        if ($httpCode === 401)
            return ["error" => ["message" => "Authentication failed. Invalid or expired token. Try again later."]];

        // Handle error with validation response
        if ($httpCode == 400)
            return ["error" => ["message" => "Validation failed: ".$response["message"]]];

        // Handle validation errors
        if ($httpCode == 200 && isset($response["message"]))
            return ["error" => ["message" => "Validation failed: ".$response["message"]]];

        // Handle url response and return {long_url: string, short_url: string}
        if ($httpCode == 200 && isset($response["results"]["long_url"]))
            return ["data" => $response];

        // Fall through to returning empty data. Should never reach here
        return ["error" => ["message" => "Undefined error ".$response]];

   }
}