<?php

namespace Tingg\Checkout;

class Mock
{
    const SERVICE_CODE = "JOHNDOEONLINE";
    const IV_KEY = "mmYpFwyyFgifeQcu";
    const SECRET_KEY = "LDNAQW0w2FFkzDeY";
    const ACCESS_KEY = "2w0FQAFDW0YW0QDAAezk2zzeL2wwYDDwQW2D2DFzDwe2zFeFLzFDDkWYkA0k";

    const CLIENT_ID ="JOHNDOEONLINE_1636532610646";

    const CLIENT_SECRET ="wUBOY0qu9eCbVMjNiAXEAUcKfIExjMBy4ugetwt9d";

    public static function PAYLOAD_ARR()
    {
        return [
            // transaction details
            "msisdn" => "+254700000000",
            "due_date" => date('Y-m-d h:i:s', strtotime('+12 hours', strtotime('now'))),
            "customer_email" => "johndoe@mail.local",
            "customer_last_name" => "Doe",
            "customer_first_name" => "John",

            "account_number" => "QWERTY",
            "invoice_number" => 'INV' . (string)strtotime('now'),
            "request_amount" => 100,
            "request_description" => "Tingg checkout tests",
            "merchant_transaction_id" => (string)strtotime('now'),

            // checkout configurations
            "service_code" => self::SERVICE_CODE,
            "prefill_msisdn" => true,
            "language_code" => "en",
            "payment_option_code" => "TREASURY",
            "charge_beneficiaries" => [
                [
                    "amount" => 1,
                    "charge_beneficiary_code" => "TAX",
                ],
                [
                    "amount" => 2,
                    "charge_beneficiary_code" => "COMMISSION",
                ],
            ],
            "country_code" => "KEN",
            "currency_code" => "KES",

            // webhooks configurations
            "callback_url" => "https://example.local/callback",
            "fail_redirect_url" => "https://example.local/callback?reason=fail",
            "success_redirect_url" => "https://example.local/callback?reason=success",
            "pending_redirect_url" => "https://example.local/callback?reason=pending",
        ];
    }
}