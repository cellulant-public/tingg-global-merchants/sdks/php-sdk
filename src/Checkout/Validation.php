<?php

namespace Tingg\Checkout;

use Carbon\Carbon;
use Tingg\Checkout\Constants as Constants;

class Validation
{
    private $payload = [];
    private $error = [];

    /**
     * @param mixed $payload
     * @throws \InvalidArgumentException
     */
    public function __construct($payload)
    {
        if (self::is_associative($payload)) $this->payload = $payload;

        if (is_string($payload) && self::is_valid_json($payload))
            $this->payload = json_decode($payload, true);

        if (empty($this->payload))
            throw new \InvalidArgumentException("Invalid payload. JSON string or associative array expected, " . gettype($payload) . " given");
    }

    public static function is_associative($value): bool
    {
        return is_array($value) && array_keys($value) !== range(0, count($value) - 1);
    }

    public static function is_valid_json($value): bool
    {
        if (is_bool($value) || is_numeric($value)) return false;
        if (strcasecmp($value, 'true') === 0 || strcasecmp($value, 'false') === 0) return false;

        // Use json_decode with associative array flag to parse objects as arrays
        $decoded = json_decode($value, true);

        return (json_last_error() == JSON_ERROR_NONE) && $decoded !== null;
    }

    public function validate()
    {
        // REQUIRED
        foreach (Constants::REQUIRED_PAYLOAD_FIELDS() as $key => $value) {
            if (!isset($this->payload[$key])) {
                $message = 'The ' . $key . ' is required';

                $value = array_key_exists($key, $this->error)
                    ? array_merge($this->error[$key], [$message])
                    : [$message];

                $this->error[$key] = $value;
            }
        }

        if (sizeof($this->error) !== 0) return ["error" => $this->error];

        // msisdn
        if (!self::is_valid_msisdn($this->payload['msisdn'], $this->payload['country_code'])) {
            $this->error['msisdn'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['msisdn']];
        }

        // due_date
        if (!self::is_valid_due_date($this->payload['due_date'])) {
            $this->error['due_date'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['due_date']];
        }

        // account_number
        if (!self::is_valid_string($this->payload['account_number'])) {
            $this->error['account_number'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['account_number']];
        }

        // request_amount
        if (!self::is_valid_amount($this->payload['request_amount'])) {
            $this->error['request_amount'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['request_amount']];
        }

        // merchant_transaction_id
        if (!self::is_valid_string($this->payload['merchant_transaction_id'])) {
            $this->error['merchant_transaction_id'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['merchant_transaction_id']];
        }

        // service_code
        if (!self::is_valid_string($this->payload['service_code'])) {
            $this->error['service_code'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['service_code']];
        }

        // country_code
        if (!self::is_valid_country_code($this->payload['country_code'])) {
            $this->error['country_code'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['country_code']];
        }

        // currency_code
        if (!self::is_valid_currency_code($this->payload['currency_code'])) {
            $this->error['currency_code'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['currency_code']];
        }

        // callback_url
        if (!self::is_valid_url($this->payload['callback_url'])) {
            $this->error['callback_url'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['callback_url']];
        }

        // fail_redirect_url
        if (!self::is_valid_url($this->payload['fail_redirect_url'])) {
            $this->error['fail_redirect_url'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['fail_redirect_url']];
        }

        // success_redirect_url
        if (!self::is_valid_url($this->payload['success_redirect_url'])) {
            $this->error['success_redirect_url'] = [Constants::REQUIRED_PAYLOAD_FIELDS()['success_redirect_url']];
        }

        // OPTIONAL
        // customer_email
        if (!self::is_valid_email($this->payload['customer_email'])) {
            $this->error['customer_email'] = [Constants::OPTIONAL_PAYLOAD_FIELDS()['customer_email']];
        }

        // customer_last_name
        if (!self::is_valid_string($this->payload['customer_last_name'])) {
            $this->error['customer_last_name'] = [Constants::OPTIONAL_PAYLOAD_FIELDS()['customer_last_name']];
        }

        // customer_first_name
        if (!self::is_valid_string($this->payload['customer_first_name'])) {
            $this->error['customer_first_name'] = [Constants::OPTIONAL_PAYLOAD_FIELDS()['customer_first_name']];
        }

        // request_description
        if (!self::is_valid_string($this->payload['request_description'])) {
            $this->error['request_description'] = [Constants::OPTIONAL_PAYLOAD_FIELDS()['request_description']];
        }

        // invoice_number
        if (!self::is_valid_string($this->payload['invoice_number'])) {
            $this->error['invoice_number'] = [Constants::OPTIONAL_PAYLOAD_FIELDS()['invoice_number']];
        }

        // prefill_msisdn
        if (!self::is_valid_bool($this->payload['prefill_msisdn'])) {
            $this->error['prefill_msisdn'] = [Constants::OPTIONAL_PAYLOAD_FIELDS()['prefill_msisdn']];
        }

        // payment_option_code
        if (!self::is_valid_string($this->payload['payment_option_code'])) {
            $this->error['payment_option_code'] = [Constants::OPTIONAL_PAYLOAD_FIELDS()['payment_option_code']];
        }

        // language_code
        if (!self::is_valid_language_code($this->payload['language_code'])) {
            $this->error['language_code'] = [Constants::OPTIONAL_PAYLOAD_FIELDS()['language_code']];
        }

        // pending_redirect_url
        if (!self::is_valid_url($this->payload['pending_redirect_url'])) {
            $this->error['pending_redirect_url'] = [Constants::OPTIONAL_PAYLOAD_FIELDS()['pending_redirect_url']];
        }

        // charge beneficiaries
        if (!self::is_valid_beneficiary($this->payload['charge_beneficiaries'])) {
            $this->error['charge_beneficiaries'] = [Constants::OPTIONAL_PAYLOAD_FIELDS()['charge_beneficiaries']];
        }

        return (sizeof($this->error) !== 0)
            ? ["error" => $this->error]
            : ["data" => $this->payload];
    }

    public static function is_valid_msisdn(string $value, $country_code): bool
    {
        // Remove anything not a number or plus sign
        $value = preg_replace('/[^0-9+]/', '', $value);

        $calling_code = null;
        foreach (Constants::SUPPORTED_COUNTRIES as $country) {
            if ($country['country_code'] === $country_code) {
                $calling_code = $country['calling_code'];
            }
        }

        // Check it is valid calling code string
        if (!$calling_code) return false;

        // Check it starts with the calling code
        if (!str_starts_with($value, '+' . $calling_code)) return false;

        // Test if the MSISDN matches the E.164 format
        return preg_match(Constants::VALID_MSISDN_REGEX, $value) === 1;
    }

    public static function is_valid_due_date($value): bool
    {
        try {
            $dateTime = Carbon::createFromFormat(Constants::VALID_DATE_FORMAT, $value, 'UTC');
            return $dateTime && $dateTime->format(Constants::VALID_DATE_FORMAT) === $value;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function is_valid_string($value): bool
    {
        return (is_string($value) && trim($value) !== "") || (is_numeric($value) && !is_nan($value));
    }

    public static function is_valid_amount($value): bool
    {
        return is_numeric($value) && !is_nan($value) && !is_infinite($value) && $value > 0;
    }

    public static function is_valid_country_code($value): bool
    {
        return in_array($value, Constants::SUPPORTED_COUNTRY_CODES, true);
    }

    public static function is_valid_currency_code($value): bool
    {
        return in_array($value, Constants::SUPPORTED_CURRENCY_CODES, true);
    }

    public static function is_valid_url($value): bool
    {
        return filter_var($value, FILTER_VALIDATE_URL) !== false;
    }

    public static function is_valid_email($value): bool
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }

    public static function is_valid_bool($value): bool
    {
        return is_bool($value) && !is_numeric($value);
    }

    public static function is_valid_language_code($value): bool
    {
        return in_array($value, Constants::SUPPORTED_LANGUAGE_CODES, true);
    }

    public static function is_valid_beneficiary($value): bool
    {
        if (!is_array($value)) return false;

        if (self::is_associative($value)) return false;

        return array_reduce($value, function ($carry, $item) {
            $valid = true;

            if (!self::is_associative($item)) $valid = false;

            if (!self::is_valid_amount($item['amount'] ?? null) || !self::is_valid_string($item['charge_beneficiary_code'] ?? null)) $valid = false;

            return $carry && $valid;
        }, true);
    }
}